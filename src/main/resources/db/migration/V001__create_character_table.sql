CREATE TABLE IF NOT EXISTS `character` (
    `id`            INT             PRIMARY KEY AUTO_INCREMENT,
    `name`          VARCHAR(64)     NOT NULL DEFAULT 'unnamed',
    `hp`            INT             NOT NULL DEFAULT 100,
    `x`             INT             NOT NULL DEFAULT 0,
    `y`             INT             NOT NULL DEFAULT 0
);
