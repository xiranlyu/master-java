package org.tw.battle.infrastructure;

import org.tw.battle.domain.ServiceConfiguration;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * @author Liu Xia
 */
public class DatabaseConnectionProvider {
    public static Connection createConnection(ServiceConfiguration configuration) throws Exception {
        Class.forName(configuration.getDriver());
        return DriverManager.getConnection(
            configuration.getUri(), configuration.getUsername(), configuration.getPassword());
    }
}
