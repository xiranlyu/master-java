package org.tw.battle.domain.commands;

import org.tw.battle.domain.CommandHandler;
import org.tw.battle.domain.CommandResponse;
import org.tw.battle.domain.repositories.CharacterRepository;

/**
 * @author Liu Xia
 */
public class GetCharacterInformationCommandHandler implements CommandHandler {
    private static final String CMD_NAME = "character-info";
    private final CharacterRepository characterRepository;

    public GetCharacterInformationCommandHandler(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public boolean canHandle(String commandName) {
        return CMD_NAME.equals(commandName);
    }

    @Override
    public CommandResponse handle(String[] commandArgs) throws Exception {
            if (commandArgs.length != 1) {
                return CommandResponse.fail("Bad command: character-info <character id>");
            }
            int id = Integer.parseInt(commandArgs[0]);
            String message = characterRepository.getCharacter(id);
            return message.equals("Bad command: character not exist") ? CommandResponse.fail(message): CommandResponse.success(message);
    }
}
