package org.tw.battle.domain.commands;

import org.tw.battle.domain.CommandHandler;
import org.tw.battle.domain.CommandResponse;
import org.tw.battle.domain.repositories.CharacterRepository;

import java.util.regex.Pattern;

/**
 * @author Liu Xia
 */
public class RenamingCharacterCommandHandler implements CommandHandler {
    private static final String CMD_NAME = "rename-character";
    private final CharacterRepository characterRepository;

    public RenamingCharacterCommandHandler(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public boolean canHandle(String commandName) {
        return CMD_NAME.equals(commandName);
    }

    @Override
    public CommandResponse handle(String[] commandArgs) throws Exception {
        if (commandArgs.length != 2) {
            return CommandResponse.fail("Bad command: rename-character <character id> <new name>");
        }
        char[] chars2 = commandArgs[0].toCharArray();
        for (char c: chars2) {
            if(!Character.isDigit(c)) {
                return CommandResponse.fail("Bad command: rename-character <character id> <new name>");
            }
        }
        int id = Integer.parseInt(commandArgs[0]);
        String name = commandArgs[1];
        char[] chars = name.toCharArray();
        if (chars.length < 3) {
            return CommandResponse.fail("Bad command: rename-character <character id> <new name>");
        } else {
            for (char c: chars) {
                if(!Character.isLetter(c) && c != '-') {
                    return CommandResponse.fail("Bad command: rename-character <character id> <new name>");
                }
            }
        }
        String message = characterRepository.renameCharacter(id, name);
        return message.equals("Bad command: character not exist") || message.equals("Bad command: rename-character <character id> <new name>")?
                CommandResponse.fail(message): CommandResponse.success(message);
    }
}
