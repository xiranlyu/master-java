package org.tw.battle.domain.repositories;

import org.tw.battle.domain.ServiceConfiguration;
import org.tw.battle.infrastructure.DatabaseConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import static org.tw.battle.infrastructure.DatabaseConnectionProvider.createConnection;

/**
 * @author Liu Xia
 */
public class CharacterRepository {
    private final ServiceConfiguration configuration;

    public CharacterRepository(ServiceConfiguration configuration) {
        this.configuration = configuration;
    }

    public int create(String name, int hp, int x, int y) throws Exception {
        try (
                Connection newConnection = createConnection(configuration);
                PreparedStatement newStatement = newConnection.prepareStatement("INSERT INTO character (`name`, `hp`, `x`, `y`) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
        ) {
            newStatement.setString(1, name);
            newStatement.setInt(2, hp);
            newStatement.setInt(3, x);
            newStatement.setInt(4, y);
            newStatement.execute();
            final ResultSet keys = newStatement.getGeneratedKeys();
            keys.next();
            return keys.getInt(1);
        }
    }

    //BELOW IS THE METHOD CREATED FOR GetCharacterInformationCommandHandler
    public String getCharacter(int id) throws Exception {
        try (
                Connection newConnection = createConnection(configuration);
                Statement newStatement = newConnection.createStatement();
        ) {
            String query = "SELECT * FROM character WHERE `id` = " + id;
            int x = 0;
            int y = 0;
            int hp = 0;
            String name = "";
            String status = "";
            ResultSet result = newStatement.executeQuery(query);
            if (result.next()) {
                id = result.getInt("id");
                hp = result.getInt("hp");
                x = result.getInt("x");
                y = result.getInt("y");
                name = result.getString("name");
                if (hp > 0) {
                    status = "alive";
                } else {
                    status = "dead";
                }
            } else {
                return "Bad command: character not exist";
            }
            return String.format("id: %d, name: %s, hp: %d, x: %d, y: %d, status: %s", id, name, hp, x, y, status);
        } catch (Exception e) {
            throw e;
        }
    }

//BELOW IS THE METHOD CREATED FOR RenamingCharacterInformationCommandHandler
    public String renameCharacter(int id, String name) throws Exception {
        try (
            Connection newConnection = createConnection(configuration);
        ) {
            if (getCharacter(id).equals("Bad command: character not exist")) {
                return "Bad command: character not exist";
            }
            String query = "UPDATE character SET `name` = ? WHERE `id`= ?;";
            PreparedStatement newStatement = newConnection.prepareStatement(query);
            newStatement.setString(1, name);
            newStatement.setInt(2, id);
            newStatement.execute();
            return String.format("Character renamed: id: %d, name: %s", id, name);
            } catch (Exception e) {
            return "Bad command: rename-character <character id> <new name>";
        }
    }
}