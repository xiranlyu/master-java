package org.tw.battle;

import org.junit.jupiter.api.Test;
import org.tw.battle.domain.CommandResponse;
import org.tw.battle.domain.Game;
import org.tw.battle.mocks.CommandHandlerWithUnhandledException;
import org.tw.battle.mocks.MockCommandHandler;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

public class GameGeneralTest {
    @Test
    void should_execute_command_once_match() {
        final String commandName = "test";
        final String expectedMessage = "Message";
        final MockCommandHandler commandHandler =
            new MockCommandHandler(commandName, true, expectedMessage);
        final Game game = new Game(Collections.singletonList(commandHandler));
        final CommandResponse response = game.execute(commandName);

        assertTrue(response.isSuccess());
        assertEquals(expectedMessage, response.getMessage());
        assertTrue(commandHandler.isCalled());
    }

    @Test
    void should_correctly_get_command_args() {
        final String commandName = "test";
        final String[] expectCommandArgs = new String[] {"arg1", "arg2", "arg3"};
        final MockCommandHandler commandHandler =
            new MockCommandHandler(commandName, true, "Message");
        final Game game = new Game(Collections.singletonList(commandHandler));
        game.execute(commandName, "arg1", "arg2", "arg3");

        assertTrue(commandHandler.isCalled());
        assertArrayEquals(expectCommandArgs, commandHandler.getCommandArgs());
    }

    @Test
    void should_return_failure_response_if_unhandled_exception_occurred() {
        final String expectedMessage = "message";
        final Game game = new Game(Collections.singletonList(new CommandHandlerWithUnhandledException(expectedMessage)));

        final CommandResponse response = game.execute("whatever");
        assertFalse(response.isSuccess());
        assertEquals("Unhandled error: " + expectedMessage, response.getMessage());
    }
}
